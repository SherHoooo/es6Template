module.exports = {
  "helpers": {
    "if_or": function (v1, v2, options) {
      if (v1 || v2) {
        return options.fn(this);
      }

      return options.inverse(this);
    }
  },
  "prompts": {
    "name": {
      "type": "string",
      "required": true,
      "message": "Project name"
    }, 
    "description": {
      "type": "string",
      "required": false,
      "message": "Project description",
      "default": "A es6 project"
    },
    "author": {
      "type": "string",
      "message": "Author"
    },
    "lint": {
      "type": "confirm",
      "message": "Use ESLint to lint your code?"
    },
    "unit": {
      "type": "confirm",
      "message": "Setup unit tests with jest?"
    },
    "style": {
      "type": "list",
      "message": "Use less or sass in your code?",
      "choices": [
        {
          "name": "Use less",
          "value": "less",
          "short": "less"
        },
        {
          "name": "Use sass",
          "value": "sass",
          "short": "sass"
        },
        {
          "name": "just use css",
          "value": "css",
          "short": "css"
        }
      ],
      "default": "sass"
    }
  },
  "filters": {
    ".eslintignore": "lint",
    ".eslintrc": "lint",
    "test/**/*": "unit",
    "src/index.less": "style:less",
    "src/index.sass": "style:sass",
    "src/index.css": "style:css"
  },
  "completeMessage": "done"
};