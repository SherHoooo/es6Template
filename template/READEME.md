# 简单的 ES6 脚手架
> 实现了热更新，可以使用 Promise 等

```shell
npm install

npm run build // build 到 dist 目录

npm run dev // 启动一个服务，实现代码热更新
```

然后访问 http://localhost:8080/ 即可

## package.json
1. "build" 清空dist目录，启动单元测试以及ESLint。并且将代码打包到dist
2. "dev" 启动一个web服务，并且监听代码更新
3. "lint"  eslint webpack config ，以及 src目录下的所有文件
4. "lint:watch" 动态监听代码ESLint
5. "test" 启动单元测试
6. "test:watch" 动态监听单元测试代码
7. "test:cover" 单元测试覆盖率
8. "clean-dist" 清空dist 目录
9. "prebuild" 预发布前准备，清空dist目录，单元测试，以及ESLint
