{{#if_eq style 'sass'}}require('./index.sass'){{/if_eq}}{{#if_eq style 'less'}}
require('./index.less'){{/if_eq}}{{#if_eq style 'css'}}
require('./index.css'){{/if_eq}}

let main = () => {
  new Promise((resolve) => {
    console.log('test promise') // eslint-disable-line no-console
    resolve()
  }).then(() => {
    let root = document.createElement('div')

    root.appendChild(document.createTextNode('hello world!!'))
    root.id = 'root'

    document.body.appendChild(root)
  })
}

main()
