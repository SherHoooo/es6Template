let path = require('path')
let HtmlWebpackPlugin = require('html-webpack-plugin')

module.exports = {
  entry: path.resolve(__dirname, 'src/index.js'),
  devtool: 'eval-source-map',
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'bundle.js'
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: 'src/index.ejs',
      minify: {
        removeComments: true,
        collapseWhitespace: true
      },
      inject: true
    })
  ],
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /(node_modules|bower_components)/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: 'env',
            plugins: ['transform-runtime']
          }
        }
      },
      {
        test: /\.(eot|svg|ttf|woff|woff2)$/,
        loader: 'file-loader?name=fonts/[name].[ext]'
      },
      {
        test: /\.(jpe?g|png|gif)$/i,
        loader: 'url-loader?limit=25000&name=img/[name].[ext]' // 设置图片不大于 25kb 时，使用 base64
      },
      {
        test: /\.ico$/,
        loader: 'url-loader?limit=65000&name=img/[name].[ext]'
      }{{#if_eq style 'sass'}},
      {
        test: /(\.css|\.scss|\.sass)$/,
        loaders: ['style-loader', 'css-loader?sourceMap', 'sass-loader?sourceMap']
      }{{/if_eq}}{{#if_eq style 'less'}},
      {
        test: /(\.css|\.less)/,
        loader: ['style-loader', 'css-loader?sourceMap', 'less-loader?sourceMap']
      }{{/if_eq}}{{#if_eq style 'css'}},
      {
        test: /\.css/,
        loader: ['style-loader', 'css-loader?sourceMap']
      }{{/if_eq}}
    ]
  }
}
