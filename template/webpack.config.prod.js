let path = require('path')
let ExtractTextPlugin = require('extract-text-webpack-plugin')
let HtmlWebpackPlugin = require('html-webpack-plugin')
let webpack = require('webpack')

module.exports = {
  entry: {
    app: path.resolve(__dirname, 'src/index.js')
    // vendors: [''] // 第三方库，打包时采用分开打包
  },
  // devtool: 'source-map', // 打包时是否生成map文件
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'js/[name].[hash].js'
  },
  plugins: [
    new ExtractTextPlugin('css/[name].[contenthash].css'),
    new HtmlWebpackPlugin({ // 自动生成 html
      template: 'src/index.ejs',
      favicon: 'src/favicon.ico',
      minify: {
        removeComments: true,
        collapseWhitespace: true,
        removeRedundantAttributes: true,
        useShortDoctype: true,
        removeEmptyAttributes: true,
        removeStyleLinkTypeAttributes: true,
        keepClosingSlash: true,
        minifyJS: true,
        minifyCSS: true,
        minifyURLs: true
      },
      inject: true
    }),
    new webpack.optimize.UglifyJsPlugin({ // 压缩js文件
      sourceMap: true,
      compress: {
        dead_code: true,
        unused: true,
        warnings: false
      }
    })
    // 第三方库分开打包
    // new webpack.optimize.CommonsChunkPlugin({ name: 'vendor', filename: 'js/vendor.bundle.js' })
  ],
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /(node_modules|bower_components)/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: 'env',
            plugins: ['transform-runtime']
          }
        }
      },
      {
        test: /\.(eot|svg|ttf|woff|woff2)$/,
        loader: 'file-loader?name=fonts/[hash].[ext]'
      },
      {
        test: /\.(jpe?g|png|gif)$/i,
        loader: 'url-loader?limit=25000&name=img/[hash].[ext]' // 设置图片不大于 25kb 时，使用 base64
      },
      {
        test: /\.ico$/,
        loader: 'file-loader?name=img/[hash].[ext]'
      }{{#if_eq style 'sass'}},
      {
        test: /(\.css|\.scss|\.sass)$/,
        loaders: ExtractTextPlugin.extract('css-loader?sourceMap&minimize!sass-loader?sourceMap')
      }{{/if_eq}}{{#if_eq style 'less'}},
      {
        test: /(\.css|\.less)$/,
        loaders: ExtractTextPlugin.extract('css-loader?sourceMap&minimize!less-loader?sourceMap')
      }{{/if_eq}}{{#if_eq style 'css'}},
      {
        test: /\.css/,
        loader: ExtractTextPlugin.extract('css-loader?sourceMap&minimize')
      }{{/if_eq}}
    ]
  }
}
